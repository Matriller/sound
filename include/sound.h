#ifndef __LOSU_SOUND_SOUND_H__
#define __LOSU_SOUND_SOUND_H__

#ifdef _WIN32
    // 编译时需要的链接指令
    // -lWinmm
    #include <windows.h>
    #include <mmsystem.h>
#endif

#include <string>
#include <vector>
#include <iostream>

namespace sound {
    using std::string;
    using std::vector;

    class MusicTask {
    private:
        string path;
        int index;
    public:
        void setPath(const char *s);
        void setPath(string & s);
        string & getPath();
        bool open();
        bool close();
        bool play(bool wait = true);
        bool stop();
        bool resume();
        bool seek(double place);
        bool seek(string place);
        double length();
        double cur();
        double getVolume();
        bool setVolume(double v);
    };
}

#endif
