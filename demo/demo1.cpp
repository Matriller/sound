#include "sound.h"

int main() {
    sound::MusicTask task;

    task.setPath("./music.mp3");
    task.open();
    task.play(false);

    Sleep(10000);
    task.stop();
    Sleep(10000);
    task.resume();
    Sleep(10000);

    task.close();

    return 0;
}
