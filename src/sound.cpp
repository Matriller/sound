#include "sound.h"

namespace sound {
    void MusicTask::setPath(const char *s) {
        path = s;
    }

    void MusicTask::setPath(string & s) {
        path = s;
    }

    string & MusicTask::getPath() {
        return path;
    }

    static size_t __index__ = 0;

    static void makename(int index, char *target) {
        snprintf(target, 512, "task%d", index);
    }

    bool MusicTask::open() {
        char name[512];
        char cmd[512];

        index = __index__;
        __index__ ++;

        makename(index, name);
        snprintf(cmd, sizeof(cmd), "open %s alias %s", path.c_str(), name);
        return mciSendStringA(cmd, NULL, 0, NULL) == 0;
    }

    bool MusicTask::close() {
        char name[512], cmd[512];

        makename(index, name);
        snprintf(cmd, sizeof(cmd), "close %s", name);
        index = 0;
        path = "";
        return mciSendStringA(cmd, NULL, 0, NULL) == 0;
    }

    bool MusicTask::play(bool wait) {
        char name[512], cmd[512];

        makename(index, name);
        if (wait)
            snprintf(cmd, sizeof(cmd), "play %s wait", name);
        else
            snprintf(cmd, sizeof(cmd), "play %s", name);
        return mciSendStringA(cmd, NULL, 0, NULL) == 0;
    }

    bool MusicTask::stop() {
        char name[512], cmd[512];

        makename(index, name);
        snprintf(cmd, sizeof(cmd), "stop %s", name);
        return mciSendStringA(cmd, NULL, 0, NULL) == 0;
    }

    bool MusicTask::resume() {
        char name[512], cmd[512];

        makename(index, name);
        snprintf(cmd, sizeof(cmd), "resume %s", name);
        return mciSendStringA(cmd, NULL, 0, NULL) == 0;
    }

    bool MusicTask::seek(double place) {
        char name[512], cmd[512];

        makename(index, name);
        snprintf(cmd, sizeof(cmd), "seek %s to %d", name, (int)(place * 1000));
        return mciSendStringA(cmd, NULL, 0, NULL) == 0;
    }

    bool MusicTask::seek(string place) {
        char name[512], cmd[512];

        makename(index, name);
        snprintf(cmd, sizeof(cmd), "seek %s to %s", name, place.c_str());
        return mciSendStringA(cmd, NULL, 0, NULL) == 0;
    }

    double MusicTask::length() {
        char name[512], cmd[512], len[512];

        makename(index, name);
        snprintf(cmd, sizeof(cmd), "status %s length", name);
        if (mciSendStringA(cmd, len, sizeof(len), 0) != 0)
            return -1;
        else {
            int output;
            sscanf(len, "%d", &output);
            return (double)output / 1000;
        }
    }

    double MusicTask::cur() {
        char name[512], cmd[512], len[512];

        makename(index, name);
        snprintf(cmd, sizeof(cmd), "status %s position", name);
        if (mciSendStringA(cmd, len, sizeof(len), 0) == 0) {
            int output;
            sscanf(len, "%d", &output);
            return (double)output / 1000;
        }
        return -1;
    }

    double MusicTask::getVolume() {
        char name[512], cmd[512], len[512];

        makename(index, name);
        snprintf(cmd, sizeof(cmd), "status %s volume", name);
        if (mciSendStringA(cmd, len, sizeof(len), 0) == 0) {
            int output;
            sscanf(len, "%d", &output);
            return (double)output / 1000;
        }
        return -1;
    }

    bool MusicTask::setVolume(double v) {
        char name[512], cmd[512];

        makename(index, name);
        snprintf(cmd, sizeof(cmd), "setaudio %s volume to %d", name, v * 1000);
        return mciSendStringA(cmd, nullptr, 0, 0) == 0;
    }
}
